﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AudioSampler
{
    public partial class Form1 : Form
    {
        private System.Media.SoundPlayer player = null;

        public Form1()
        {
            InitializeComponent();
        }

        

        //WAVEファイルを再生する
        private void PlaySound(string waveFile)
        {
            //再生されているときは止める
            if (player != null)
                StopSound();

            //読み込む
            player = new System.Media.SoundPlayer(waveFile);
            //非同期再生する
            player.PlaySync();
        }

        private void StopSound()
        {
            if(player != null)
            {
                player.Stop();
                player.Dispose();
                player = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows User AccounControl.wav");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Notify Messaging.wav");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Background.wav");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Foreground.wav");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Device Connection.wav");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Device Ejection.wav");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Device Connection Fail.wav");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Message Nudge.wav");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Notify Calendar.wav");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Voice Recognition On.wav");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Voice Recognition Off.wav");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Voice Recognition Sleep.wav");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Recycling.wav");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Navi Start.wav");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Discovery Feed.wav");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            PlaySound(@"..\..\sound\Windows Popup Block.wav");
        }
    }
}
